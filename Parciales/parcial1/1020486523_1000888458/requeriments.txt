Jamir Moreno Salazar
Daniel Valle Jaramillo

Requerimientos para la configuración óptima del proyecto.


Primero realizamos un Fork al repositorio correspondiente. Esto se hace en gitlab, en la opcion "Fork", con esto se nos hace una copia a dicho repositorio, luego le damos en la opcion "Code" y seleccionamos "Clone with HTTPS". Por ultimo nos vamos a la terminal de gitbash y escribimos el comando "git clone" y pegamos el link que previamente copiamos. Si deseamos podemos crear una carpeta, para esto usamos la opcion mkdir y para entrar a esta con el comando cd.
Ya con esto, en el Visual Studio code abrimos nuestro respositorio dandole  a File- Open Folder.

1. Ahora para instalar un entorno virtual,  en Visual Studio code  le damos a Terminal- New Terminal. Alli introducimos el siguiente comando:

-pip install virtualenv.

2. Para crear el entorno virtual en la terminal ponemos el comando:

-python -m venv "nombre del entorno virtual" ( Se recomienda que el nombre del entorno virtual sea "venv" para evitar confusiones)

3.Para activar el entorno virtual en la terminal introducimos: ( Cabe recalcar que este proceso es para Windows)

.\venv.\Scripts\activate

Par Ubuntu:

source .\venv\bin\activate

4.Con el respositorio abierto,  si queremos actualizar el mismo entonces en el Visual Studio code vamos a la opcion Terminal- New Terminal. Alli introducimos los siguientes comandos:

git fetch upstream
git checkout main
git merge upstream/main

Con esto nuestro repositorio se actualizará.

5. Para instalar las librerías y dependencias para le correcta ejecución del proyecto. Nos vamos de nuevo a Terminal- New terminal.
Para nuestro caso necesitamos las  librerias numpy y matplotlib. Su proceso de instalación es:
pip install numpy
pip install matplotlib