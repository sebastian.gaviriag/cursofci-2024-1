import numpy as np
import matplotlib.pyplot as plt
from scipy.integrate import odeint

class RKG:
    """Se definen las condiciones para ejecutar cualquier orden de RGK;
        dt= define el tamaño del paso
        func= define la EDO, se debe definir como func(t,y), donde t es el parametro
        independiente y y dependiente
        y0= define la condicion inicial de la variable depedeniente
        t0= define la condicion inicial de la variable independiente
        yf= define hasta donde se van a calcular las soluciones
        A= es la matrix de coeficente a_ij, se debe tener cuidado al definirla, su tamaño
        es order*order, la primera fila es de ceros, la segunda fila corresponde a los coeficientes
        a_2j, pero j=1, ya que la condicion(revisar 
        https://en.wikipedia.org/wiki/Runge%E2%80%93Kutta_methods#The_Runge%E2%80%93Kutta_method), es que
        j<i, asi por ejemplo para la tercera fila se tienen los coeficiente: a_31, a_32. y así hasta i,
        donde i=1,...,order
        c_s= es un vector que contiene los nodos, caracterizados como c_i(i=2,...,order), el primer elemento
        es siempre por defecto igual a 0 
        b_s= (b_i, i=1,..,order)es el vector que contiene los pesos que multiplicaran cada orden de 
        runge kutta, se debe tener en cuenta que sum(b_i)=1 para garantizar convergencia
        order= define el orden del runge kutta
        """

    def __init__(self,dt,func,y0,t0,yf,A,c_s,b_s,order):
        
        self.dt=dt
        self.f=func
        self.y0=y0
        self.t0=t0
    
        self.yf=yf
        self.A=A
        self.c_s=c_s
        self.b_s=b_s
        self.order=order


    def rkg(self):
        K_s = np.zeros((self.order)) #se crea un array que contiene los K_i ordenes de RGK
        
        for i in range(self.order): #se crea cada k_i de forma vectorial+iterativa
            k=self.f(self.t0+self.c_s[i]*self.dt,self.y0 + np.sum(A[i]*K_s)*self.dt)

        
            K_s[i]=k #se almacena cada k_i en el array K_s

        fout= self.y0+ self.dt * np.sum(K_s*self.b_s) #se calcula y_n+1
        
        
        return fout

    def soluciones(self):#metodo para calcular las soluciones
        self.num_points=int(self.yf/self.dt) #se calcula #de puntos
        self.t=np.linspace(0,self.yf,self.num_points) 
         
        self.Y=np.zeros((2,self.num_points)) #aca se almacenaran las soluciones
        self.Y[0,0]=self.y0 #se almacena la condiciones inicial de y0
        self.Y[1,0]=self.t0 #se almacena la condiciones inicial de t0

        for i in range(self.num_points-1):#Calculo solucion para cada t_i,actualiza y0, almacena valores
            yout=self.rkg()
            
            self.Y[0,i+1]=yout
            self.y0=yout

            self.Y[1,i+1]=self.t[i+1]
            self.t0=self.t[i]
        return self.Y
    
class RKA(RKG):#clase que hereda RKG
    """Se usan los PPO para heredar metodos de otras clases y reusar codigo,
    lamentablemnete por falta de tiempo y experticia, todo redefinir los metodos
    en esta clase, no de manera drastica.
    Esta clase trabaja con una funcion que contiene 3 edos acopladas, para solucionarlas
    por RKG
    """
    def __init__(self, dt, func, y0,t0, yf, A, c_s, b_s, order):
        super().__init__(dt, func, y0, t0,yf, A, c_s, b_s, order)

        self.num_points=int(self.yf/self.dt)
        self.t=np.linspace(0,self.yf,self.num_points)
        self.y = np.zeros((3, self.num_points))
        self.y[:,0] = self.y0
    

    def rkg(self):
            
            K_s1 = np.zeros((self.order))
            K_s2 = np.zeros((self.order))
            K_s3 = np.zeros((self.order))

        
            for i in range(self.order):
                k1=self.f(self.t0+self.c_s[i]*self.dt,self.y0 + np.sum(A[i]*K_s1)*self.dt)[0]
                k2=self.f(self.t0+self.c_s[i]*self.dt,self.y0+ np.sum(A[i]*K_s2)*self.dt)[1]
                k3=self.f(self.t0+self.c_s[i]*self.dt,self.y0 + np.sum(A[i]*K_s3)*self.dt)[2]

                K_s1[i]=k1
                K_s2[i]=k2
                K_s3[i]=k3

            fout1= self.y0[0]+ self.dt * np.sum(K_s1*self.b_s)
            fout2= self.y0[1]+ self.dt * np.sum(K_s2*self.b_s)
            fout3= self.y0[2]+ self.dt * np.sum(K_s3*self.b_s)
            salida=np.array([fout1,fout2,fout3])
            return salida
    
    def soluciones(self):
        self.num_points=int(self.yf/self.dt)
        self.t=np.linspace(0,self.yf,self.num_points)
        
        self.Y=np.zeros((len(self.y0),self.num_points))# matriz 3*#puntos
        self.Y[:, 0]=self.y0
        
        for i in range(self.num_points-1):
            yout=self.rkg()
            self.Y[:,i+1]=yout
            self.y0=yout
                


        return self.Y 
        
        

            

   
def funcion(x,y): #edo a solucionar 
    dy= x-y
    return dy


h=0.01 #tamaño del paso
x_0=0.0 #condiciones inicial "t0"
y_0=0.0 #condicion inicial y0

xf=1 #punto final "yf"
num_points=int(xf/h) # esto lo defino afuera para considerar la solucion analitica
t=np.linspace(0,xf,num_points)

#A continuación considero los coeficiente para RGK-4
c_s=np.array([0,1/2,1/2,1]) #vector de nodos
b_s=np.array([1/6,1/3,1/3,1/6]) #vector de pesos
A= np.array([[0,0,0,0],[1/2,0,0,0],[0,1/2,0,0],[0,0,1,0]]) #matriz a_ij

try1=RKG(h,funcion,y_0,x_0,xf,A,c_s,b_s,4)
points=try1.soluciones() #para graficar

y_analitica2 = t - 1 + np.exp(-t) * (y_0 - t[0] + 1) #solución analitica y'=x-y


plt.figure(figsize=(10, 6))

# Subtrama 1
plt.subplot(2, 1, 1)
plt.plot(points[1], points[0], color='blue')
plt.xlabel('x')
plt.ylabel('y')
plt.title('RGK4')


# Subtrama 2
plt.subplot(2, 1, 2)
plt.plot(points[1], np.abs(points[0]-y_analitica2), color='green',label='Diferencia Absoluta')
plt.xlabel('x')
plt.ylabel('|Solución Numérica - Solución Analítica|')
plt.title('Convergencia de la Solución Numérica hacia la Solución Analítica')

# Mostrar las gráficas
plt.legend()
plt.grid(True)
plt.tight_layout()
plt.show()


