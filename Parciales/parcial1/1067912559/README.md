# Parcial #1: Física Computacional 1



## Que hace el proyecto 
Este es el seguimiento parcial #1, consiste en la verificación de las habilidades de configuración de un entorno de trabajo para la creación de un programa en Python constituido por 3 clases: 
* RKG: Runge Kutta Generalizado el cuál corresponde a una generalización del metodo de euler para resolver sístemas de ecuaciones diferenciales basandose en un método de multipaso.
Este sistema se probará en una ecuación diferencial sencilla comparando su solución con la solución exacta. 

* RKGa: Runge Kutta Generalizado Acoplado, este resolverá, tomando como herencia algunos métodos y atributos de la clase anterior, un sistema de ecuaciones diferenciales acopladas 

* Solución númerica de ecuaciones diferenciales **Ecuación de Lorenz**: Se resuelve el sístema de ecuaciones diferenciales de lorenz a partir del codigo generado en RKGa. 

Finalmente se entrega, además de los codigos anteriores, un archivo .py para la ejecución del programa y las imágenes X vs Y, X vs Z, Y vs Z, y una figura en 3D para cada una de las condiciones dadas en el problema de Lorenz

## Utilidad
Esta simulación es beneficiosa para investigadores, estudiantes interesados en comprender a mayor profundidad el concepto de la implementación de la programación orientada a objetos en relación con la física y la programación académica. Debido a la capacidad interactiva del codigo, es posible modificar ciertos parámetros físicos permitiendo explorar una variedad de escenarios del mismo fenómeno y concepto.

## Como empezar a usar este repositorio

- Clona este repositorio en tu máquina local utilizando el siguiente comando:
git clone [https://gitlab.com/valentinalobo/parcial01_fc1.git]

cd existing_repo
git remote add origin https://gitlab.com/valentinalobo/parcial01_fc1.git
git branch -M main
git push -uf origin main

- instala las dependencias necesarias ejecutando el siguiente comando en la raíz del proyecto: 
pip install -r requirements.txt

## Resultados: 
Algunos ejemplos de los resultados que puede encontrar se encuentran en las carpetas: 
* Entregable RKG 
* Entregable SolNumerica

## Autores y contribución
Este trabajo se realizó en equipo por las siguientes contribuyentes: 
Sara Carvajal (saraa.carvajal@udea.edu.co) ,  Valentina Lobo (valentina.lobo@udea.edu.co).
