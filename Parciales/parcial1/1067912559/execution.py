import matplotlib.pyplot as plt
import numpy as np
from clases import RKG, RGKA

n= 4
xi= 0
yi= 1
xfinal=10

xi_= 0
yi_= 0

h= 0.0001
bfun= [0.5,0,0.5,0,0,1]
om=[1/6,2/6,2/6,1/6]
a = [0,0.5,0.5,1]

def func(x,y):
    return x-y

def sol_analitica(x):
    return x-1+2*np.exp(-x)


prueba= RKG(func,xfinal,h,n,a,bfun,om, yi,xi)
xs,ys= prueba.soluciones()
#print(ys) 

prueba2= RKG(func,xfinal,h,n,a,bfun,om, yi_,xi_)
xs1,ys1= prueba2.soluciones()
#print(ys) 

y_exact=sol_analitica(xs)
y_exact_=sol_analitica(xs1)

plt.plot(xs, ys, label="Solución RGK4")
plt.plot(xs,y_exact, label="Solución Exacta")
plt.xlabel("x")
plt.ylabel("y")
plt.legend()
plt.grid()
plt.title("Función comparativa RGK4 con $x_0=0$, $y_0=1$")
#plt.savefig("Entregableparte2c1.png")
plt.show()


plt.plot(xs1, ys1, label="Solución RGK4",color="red")
plt.plot(xs1,y_exact_, label="Solución Exacta",color="green")
plt.xlabel("x")
plt.ylabel("y")
plt.legend()
plt.grid()
plt.title("Función comparativa RGK4 con $x_0= 0$, $y_0=0$")
plt.show()
#plt.savefig("Entregableparte2c2.png")


#Resolvamos el siguiente sistema de ecuaciones:
def lorenz(t, x, y, z, sigma, rho, beta):
    dxdt = sigma * (y - x)
    dydt = x * (rho - z) - y
    dzdt = x*y - beta*z
    return dxdt, dydt, dzdt

#Para las condiciones inciales de:


tfinal = 100
paso = 0.001 #No modificar porfa 
orden = 4
sigma = 10
rho = 28
beta = 8/3
x0 = 0
y0 = 0
z0 = 0

prueba = RGKA(lorenz, tfinal, paso, orden, a, bfun, om, sigma, rho, beta,  x0, y0, z0)
xs, ys, zs = prueba.soluciones_acopladas()

#Gráfica 3D
fig = plt.figure()
ax = fig.add_subplot(111, projection='3d')
ax.plot(xs, ys, zs, lw=0.5)
ax.set_xlabel("Eje X")
ax.set_ylabel("Eje Y")
ax.set_zlabel("Eje Z")
ax.set_title(f"Atractor de Lorenz \n con condiciones inciales xo = {x0}, yo = {y0},zo = {z0} \n sigma = {sigma},rho = {rho}, beta = {beta}")
plt.savefig("Grafica3D1.png")
plt.show()

#Para las condiciones inciales de:

sigma1= 10
rho1= 28
beta1 = 8/3
x01 = 0
y01 = 0.01
z01 = 0

prueba1 = RGKA(lorenz, tfinal, paso, orden, a, bfun, om, sigma1, rho1, beta1,  x01, y01, z01)
xs1, ys1, zs1 = prueba1.soluciones_acopladas()

#Gráfica 3D
fig = plt.figure()
ax = fig.add_subplot(111, projection='3d')
ax.plot(xs1, ys1, zs1, lw=0.5)
ax.set_xlabel("Eje X")
ax.set_ylabel("Eje Y")
ax.set_zlabel("Eje Z")
ax.set_title(f"Atractor de Lorenz \n con condiciones inciales xo = {x01}, yo = {y01},zo = {z01} \n sigma = {sigma1},rho = {rho1}, beta = {beta1}")
#plt.savefig("Grafica3D2.png")
#plt.show()

sigma2= 16
rho2= 45
beta2 = 4
x02 = 0
y02 = 0
z02 = 0

prueba2= RGKA(lorenz, tfinal, paso, orden, a, bfun, om, sigma2, rho2, beta2,  x02, y02, z02)
xs2, ys2, zs2 = prueba2.soluciones_acopladas()

#Gráfica 3D
fig = plt.figure()
ax = fig.add_subplot(111, projection='3d')
ax.plot(xs2,ys2, zs2, lw=0.5)
ax.set_xlabel("Eje X")
ax.set_ylabel("Eje Y")
ax.set_zlabel("Eje Z")
ax.set_title(f"Atractor de Lorenz \n con condiciones inciales xo = {x02}, yo = {y02},zo = {z02} \n sigma = {sigma2},rho = {rho2}, beta = {beta2}")
#plt.savefig("Grafica3D3.png")
#plt.show()

y03 = 0.2

prueba3= RGKA(lorenz, tfinal, paso, orden, a, bfun, om, sigma2, rho2, beta2,  x02, y03, z02)
xs3, ys3, zs3 = prueba3.soluciones_acopladas()

#Gráfica 3D
fig = plt.figure()
ax = fig.add_subplot(111, projection='3d')
ax.plot(xs3,ys3, zs3, lw=0.5)
ax.set_xlabel("Eje X")
ax.set_ylabel("Eje Y")
ax.set_zlabel("Eje Z")
ax.set_title(f"Atractor de Lorenz \n con condiciones inciales xo = {x02}, yo = {y03},zo = {z02} \n sigma = {sigma2},rho = {rho2}, beta = {beta2}")
#plt.savefig("Grafica3D4.png")
plt.show()

#gráfico X vs Y 
fig, ax =plt.subplots(2,2)

ax[0,0].plot(xs,ys)
ax[0,1].plot(xs1,ys1, color="darkorange")
ax[1,0].plot(xs2,ys2)
ax[1,1].plot(xs3,ys3, color="purple")
ax[0,0].set_title("Caso 1 X=0,Y=0,Z=0")
ax[0,1].set_title("Caso 2 X=0,Y=0.01,Z=0")
ax[1,0].set_title("Caso 3 X=0,Y=0,Z=0")
ax[1,1].set_title("Caso 1 X=0,Y=0.001,Z=0")
fig.suptitle("Gráfico comparativo X VS Y")
plt.show()
fig.savefig("XvsY.png")

#gráfico X vs z 
fig, ax =plt.subplots(2,2)

ax[0,0].plot(xs,zs)
ax[0,1].plot(xs1,zs1, "g")
ax[1,0].plot(xs2,zs2)
ax[1,1].plot(xs3,zs3, "r")
ax[0,0].set_title("Caso 1 X=0,Y=0,Z=0")
ax[0,1].set_title("Caso 2 X=0,Y=0.01,Z=0")
ax[1,0].set_title("Caso 3 X=0,Y=0,Z=0")
ax[1,1].set_title("Caso 1 X=0,Y=0.001,Z=0")
fig.suptitle("Gráfico comparativo X VS Y")
plt.show()
fig.savefig("XvsZ.png")


#gráfico X vs z 
fig, ax =plt.subplots(2,2)

ax[0,0].plot(ys,zs)
ax[0,1].plot(ys1,zs1, color="aqua")
ax[1,0].plot(ys2,zs2)
ax[1,1].plot(ys3,zs3, color="purple")
ax[0,0].set_title("Caso 1 X=0,Y=0,Z=0")
ax[0,1].set_title("Caso 2 X=0,Y=0.01,Z=0")
ax[1,0].set_title("Caso 3 X=0,Y=0,Z=0")
ax[1,1].set_title("Caso 1 X=0,Y=0.001,Z=0")
fig.suptitle("Gráfico comparativo X VS Y")
fig.savefig("YvsZ.png")
plt.show()






