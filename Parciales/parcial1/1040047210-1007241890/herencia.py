import numpy as np
import matplotlib.pyplot as plt
from clases import RKG

if __name__=="__main__":

    def f(t, y):   # función a integrar de la forma y'(t) = f(t,y(t))
      return t-y

    a_coefficients = [
      [0],
      [1/2, 0],
      [0, 1/2, 0],
      [0, 0, 1, 0]
      ]
    b_coefficients = [1/6, 1/3, 1/3, 1/6]
    c_coefficients = [0, 1/2, 1/2, 1]
    h = 0.00001
     #estas son las condiciones iniciales (y0)
    tf = 5
    s = 4
    y0 = 0
    t0 = 0
    rungekutta1 = RKG(f,y0, tf, h, s, c_coefficients, a_coefficients, b_coefficients)
    t_values, y_values = rungekutta1.soluciones(t0, rungekutta1.y)
    
    c=1  #constante de la solcion analítica según nuestras condiciones iniciales

    y_values_analitica = np.array(t_values) - 1 + c / np.exp(np.array(t_values))

    # Gráfica de la convergencia con respecto a la solución analítica

    error= abs(y_values_analitica-y_values)

    fig, axs = plt.subplots(2)
# Graficar en el primer subplot
    axs[0].plot(t_values, y_values, label="Solución numérica Runge-Kutta 4")
    axs[0].set_xlabel('t')  # Corrección aquí
    axs[0].set_ylabel('y')  # Corrección aquí
    axs[0].plot(t_values, y_values_analitica, label="Solución analítica")
    axs[0].set_title('Soluciones a la ecuacion diferencial dy/dt=t-y')
    axs[0].grid()
    axs[0].legend() 
# Graficar en el segundo subplot
    axs[1].plot(t_values, error)
    axs[1].set_xlabel('t') 
    axs[1].set_ylabel('|yanalítica - ynumérica|')  
    axs[1].set_title('Convergencia')
    axs[1].grid()
    # Ajustar diseño
    plt.tight_layout()
# Mostrar gráficas
    plt.savefig('C:/Users/Hogar/Desktop/Parciales_FC1/parcial1/graficas/x vs y e convergencia')
    plt.show()


