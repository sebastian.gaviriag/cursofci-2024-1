import numpy as np
import matplotlib.pyplot as plt
from clases import RKG
from clases import RKGA

if __name__ == "__main__":

    #funciones de lorentz
    def system_of_equations(t, y):
        #sigma = 10.
        #ro = 28.
        #beta = 8./3.
        sigma = 16.
        ro = 45.
        beta = 4.
        dx_dt = sigma*(y[1]-y[0])
        dy_dt = y[0]*(ro-y[2])-y[1]
        dz_dt = y[0]*y[1]-beta*y[2]
        return [dx_dt, dy_dt, dz_dt]

    # Coeficientes para el método de Runge-Kutta de cuarto orden
    c_a = [
        [0],
        [1/2, 0],
        [0, 1/2, 0],
        [0, 0, 1, 0]
    ]
    c_b = [1/6, 1/3, 1/3, 1/6]
    c_c = [0, 1/2, 1/2, 1]
    h = 0.0001
    tf = 100
    s = 4
    #y0 = [0., 0., 0.]  #Condiciones 1 (se usarán con sigma=10, rho=28, beta=8/3, son las mismas para las condiciones 3 (se usa sigma=16, rho=45, beta=4)
    #y0 = [0., 0.01, 0.] #Condiciones 2 (se usa sigma=10, rho=28, beta=8/3)
    y0 = [0., 0.001, 0.] #Condiciones 4 (se usa sigma=16, rho=45, beta=4)
    t0 = 0

    rungekutta2 = RKGA(system_of_equations, y0, tf, h, s, c_c, c_a,c_b ) #uso de la clase RKGA (la cual usa herencia)
    k_x,k_y,k_z = rungekutta2.ks_sis(rungekutta2.y,t0)
    #print(k_x,k_y,k_z)
    
    t_values, x_values, y_values, z_values = rungekutta2.sol_sistem(t0)
    #print(x_values, y_values,z_values)


    fig = plt.figure()
    ax = fig.add_subplot(111, projection='3d')
    ax.plot(x_values, y_values, z_values, label='atractor')
    ax.set_xlabel('X')
    ax.set_ylabel('Y')
    ax.set_zlabel('Z')
    ax.legend()
    #plt.savefig('C:/Users/Hogar/Desktop/Parciales_FC1/parcial1/graficas/grafica3d') #está linea se usó temporalmente para ir guardando más facilmente las gráficas
    plt.show()



    fig, axs = plt.subplots(3)

# Graficar en el primer subplot
    axs[0].plot(x_values, y_values)
    axs[0].set_title('x vs y')

# Graficar en el segundo subplot
    axs[1].plot(x_values, z_values)
    axs[1].set_title('x vs z')

# Graficar en el tercer subplot
    axs[2].plot(y_values, z_values)
    axs[2].set_title('y vs z')

# Ajustar diseño
    plt.tight_layout()

    #plt.savefig('C:/Users/Hogar/Desktop/Parciales_FC1/parcial1/graficas/graficas2d') #está linea se usó temporalmente para ir guardando más facilmente las gráficas

# Mostrar gráficas
    plt.show()
