import numpy as np
import matplotlib.pyplot as plt
from scipy.integrate import odeint
from RungeKutta import RKG, RKGa

if __name__ == '__main__':
    #Definimos la función derivada correspondiente a la EDO
    def f(t, y):
       return t - y
    
    #Definimos los coeficientes del método de Runge-Kutta para el orden 4
    s = 4
    a = [0, 0.5, 0.5, 1]
    b = [1/6, 1/3, 1/3, 1/6]
    c = [0, 0.5, 0.5, 1]
    
    #Definimos las condiciones iniciales para la EDO
    x0 = 0
    xf = 1
    y0 = 0
    h = 0.00001

    #Creamos una instancia de la clase RKG y ploteamos
    rkg = RKG(s, a, b, c)
    sol= rkg.soluciones(f, x0, y0, xf, h)
    rkg.plots(sol)
    rkg.convergencia(f, x0, y0, xf)
    
    #Definimos la función derivada para las ecuaciones de Lorenz
    def Lorenz(t, ys, args):
        x, y, z = ys
        sigma, rho, beta = args
        dxdt = sigma * (y - x)
        dydt = x * (rho - z) - y
        dzdt = x * y - beta * z
        return dxdt, dydt, dzdt
    
    #Definimos las condiciones iniciales y los parámetros para las ecuaciones acopladas
    t0 = 0
    tf = 100
    h = 0.001
    Params = [[10, 28, 8/3], 
               [16, 45, 4]]
    y_0 = [[0, 0, 0], [0, 0.01, 0], [0, 0.001, 0]]

    #Creamos una instancia de la subclase RKGa
    rkga = RKGa(s, a, b, c)
    
    #Procedemos a graficar para cada situación en las ecuaciones de Lorenz
    Sol = rkga.soluciones(Lorenz, Params[0], t0, y_0[0], tf, h)
    rkg.plots(Sol)
    rkg.plots(Sol, "3d")
    
    Sol1 = rkga.soluciones(Lorenz, Params[0], t0, y_0[1], tf, h)
    rkg.plots(Sol1)
    rkg.plots(Sol1, "3d")
    
    Sol2 = rkga.soluciones(Lorenz, Params[1], t0, y_0[0], tf, h)
    rkg.plots(Sol2)
    rkg.plots(Sol2, "3d")
    
    Sol3 = rkga.soluciones(Lorenz, Params[1], t0, y_0[2], tf, h)
    rkg.plots(Sol3)
    rkg.plots(Sol3, "3d")