import numpy as np
import matplotlib.pyplot as plt

#+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
class RKG:
    def __init__(self,function: callable, initial_conditions:np.ndarray, x_final:float, step:float, func_params:list, a:np.ndarray ,b:np.ndarray,c:np.ndarray) -> None:
        '''
        Here is defined the atributes of a RKG object, that is, an object with the neccesary information
        to solve a problem of ODE using an Explicit Runge-Kutta method of whatever order given the 
        neccesary coefficients to compute it and all the coupled ecuations.

        Params:
            function: is a function that define the ODE written in explicit form that returns [dy/dx]
                      for a first order ODE. For coupled ODEs it returns [dy1/dx, dy2/dx, ...,dyn/dx].
                      A function must have the structure:

                      def func(x:float ,y:list or float ,params:list):
                        .
                        .
                        .
                        return [dy1/dx, dy2/dx, ...,dyn/dx]
            
            initial_conditions: for a single differential equation the list of initial conditions must
                                be: [x0,y0] where x0 is the initial independt varible and y0 is the initial
                                value of the function. For system of coupled ODEs, the list must be:
                                [x0,[y1_0, y2_0, ... , yn_0]]

            x_final: is the point where finish the integration of ODE

            step: is the distance between x_n and x_n+1 (h)

            func_params: posible list of parameters for the function that define the diferencial equation,
                         if the function doesn't need parameters it must be an empty list [].

            a,b,c: a is a matrix, b and c are vectos, and together form the Butcher Tableau

                   0 |0    0   0  .    .    .   0
                   c2|a21  0   0  .    .    .   0
                   c3|a31 a31  0  .    .    .   0
                   . | .   .   .  .             .
                   . | .   .   .       .        .
                   . | .   .   .            .   .
                   cs|as1 as2 as3 . . . as,s-1  0
                   --|-----------------------------
                     |b1  b2  b3  . . . bs-1    bs
        '''
        self.func = function
        self.x0 = initial_conditions[0]
        self.y0 = np.array(initial_conditions[1]).reshape(-1)
        self.h = step
        self.x = np.arange(self.x0,x_final+step,step)
        self.y = np.zeros((len(self.y0),len(self.x)))
        self._a = a
        self._b = b
        self._c = c
        self.func_params = func_params
        #-------------------------------------------------------------------------------
        self.x[0] = self.x0   # set initial value for independent varible
        self.y[:,0] = self.y0 # set initial value(s) for dependent(es) varible(s)
        self.solved = False   # keep information about the system has been solved or not

    def ks(self,i):
        '''
        This method, calculates the Ki for each step, in the RKs we must find K1,...,Ks for each step in the iteration method
        In this case, they are stoes in  nxs matrix, where:
        
        Each row represents all the k_i values for one dependent varible at i step.                                
        '''
        k = np.zeros((len(self.y0),len(self._b)))  # initialice the matrix to store ks values
        
        for j in range(len(self._b)):
            k[:,j] = self.func(self.x[i]+self.h*self._c[j],
                               self.y[:,i] + self.h*np.dot(k,self._a[j]),
                               self.func_params)
        return k
    
    
    def soluciones(self):
        '''
        We store all the solution of the n dependent variables in a nxN matrix, where n in the amount of dependent variables
        and N is the amount of steps from x0 to xf.
        
        Which means, each row is the solution for one dependent variable step by step.
        
        This methos calls the previous method to calculate the Ki values for each step and then use it here to calculate the solutions.
        '''
        for i in range(0,len(self.x)-1):
            k = self.ks(i)
            self.y[:,i+1] = self.y[:,i] + self.h*np.dot(k,self._b)
        
        self.solved = True  # change the status of solved of the problem
        return self.y
#+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++     
class RKGA(RKG):
    '''
    This class, takes the Generalized Runge Kutta class to inherit its properties, now, given some specific
    coefficients, in the arrays c,b, and the matrix a, as follows:
    
                    0 | 0    0    0    0                0 | 0    0    0   0
                   1/2|1/2   0    0    0                c2|a21   0    0   0
                   1/2| 0   1/2   0    0        -->     c3|a31  a31   0   0 
                    1 | 0    0    1    0                c4|a41  a42  a43  0
                   ---|------------------              ---|-----------------
                      |1/6  1/3  1/3  1/6                 | b1   b2   b3  b4
    
                    Using this coefficients, the Generalized Runge Kuta method, becomes RK4
                    In this cased called RKGA.
    
    Params:
            function: is a function that define the ODE written in explicit form that returns [dy/dx]
                      for a first order ODE. For coupled ODEs it returns [dy1/dx, dy2/dx, ...,dyn/dx].
                      A function must has the structur:

                      def func(x:float ,y:list ,params:list):
                        .
                        .
                        .
                        return [dy1/dx, dy2/dx, ...,dyn/dx]
            
            initial_conditions: for a single differential equation the list of initial conditions must
            be: [x0,y0] where x0 is the initial independt varible and y0 is the initial value of the function.
            For system of coupled ODEs, the list must be: [x0,[y1_0, y2_0, ... , yn_0]]

            x_final: is the point where finish the integration of ODE

            step: is the distance between x_n and x_n+1 (h)

            func_params: posible list of parameters for the function that define the diferencial equation,
                         if the function doesn't need parameters it must be an empty list [].
                    
            Note: Here we do no ask for the coefficients as we did on the father calss RKG, becaused as we said before
            this ones are taken by defect on the RK4 method.         
    '''
    a_RK4 = np.array([[0  ,0  ,0,0],           #4x4 matrix with a's coefficents of RK4
                      [1/2,0  ,0,0],
                      [0  ,1/2,0,0],
                      [0  ,0  ,1,0]])
        
    b_RK4 = np.array([1/6,1/3,1/3,1/6])        # array with b's coefficents for RK4
    c_RK4 = np.array([0,1/2,1/2,1])            # array with c's coefficents for RK4
    
    def __init__(self, function: callable , initial_conditions: np.ndarray, x_final: float, step, func_params, a = a_RK4,b = b_RK4, c = c_RK4):
        super().__init__(function, initial_conditions, x_final, step, func_params,a,b,c)

        
    def plot(self, analytic_sol:callable=lambda x:0,value:bool = False) ->None:
        '''
        In case you want to compare the analytic solutions to the numeric one, you can add a function which returns the analitic
        solutions for each yi (dependedent variables fot the coupled system) in a tuple in the same order, the variable 
        'function' does, and then set 'value' as True. 
        
        params:
            analitic_sol: (optional) callable which returns the analytic sol for the independet variables given x, to compare
            the analyitic solution with the numerical one, this analitic solution, must stricltly return the analytic solution for
            all the independent variables to work correclty.
            
            value: boolean, to specify if you wan to compare wiht analytic solution or not.
            it not. just don't call the method with the argument value nor analytic_sol, just object.plot()
        
        Returns:
            plots all solutions separately, then the error in respect to the analytic solution if provided.
        '''
        plt.rcParams['font.size'] = 12
        self.soluciones()
        
        #To plot the y_i solutions and respective errros if given their anlytic solution.
        if value:
            fig, ax = plt.subplots(ncols=len(self.y0), nrows=2, figsize=(10,9))
            fig.tight_layout(pad=3, h_pad=4.5,w_pad=2.5)
            plt.grid()
            
            for i in range(len(self.y0)):
                ax[i].plot(self.x, self.y[i],label = f'Numeric sol $y_{i}$')
                ax[i].set_xlabel('$x$'), ax[i].set_ylabel(f'$y_{i}$'),ax[i].set_title(f'Numerical solution $y_{i}$')
                
                
                # In case the user wants to compare anlytic solution.
                difference = np.abs(self.y - np.array(analytic_sol(self.x)))
                ax[len(self.y0) + i].scatter(self.x, difference[i], label = f'Difference $y_{i}$', alpha = 0.7, marker='.')
                ax[len(self.y0) + i].set_xlabel('$x$'), ax[len(self.y0) + i].set_ylabel(f'Error'),ax[len(self.y0) + i].set_title(f'Difference (Error)\n|Numeric $y_{i}$- Analytic $y_{i}|$')
                plt.legend()
                
           
            plt.savefig('./Plot_entregable_23.png')
            
        #to plot hte y_i solutions
        else:
            fig, ax = plt.subplots(ncols=len(self.y0), nrows=1, figsize=(10,9))
            fig.tight_layout(pad=3,w_pad=2.5)
            
            for i in range(len(self.y0)):
                ax[i].plot(self.x, self.y[i],label = f'Numeric sol $y_{i}$')
                ax[i].set_xlabel('$x$'), ax[i].set_ylabel(f'$y_{i}$'),ax[i].set_title('Numerical solution')
            
                plt.legend()
                plt.grid()
            
            plt.savefig('./Plot_entregable_23.png')
    
    def plot3D(self,name:str,title:str,parms_names:list):
        '''
        This method generates a figure for a 3D problem with 4 subplots (x,y),(x,z),(y,z),(x,y,z)

        parameters:
                name: it is a string with the name to save the figure.
                title: it is a string with figure's title.
                params_names: list of string with the name of the parameters of the function
        '''
        if self.solved:
            fig = plt.figure(figsize=(12,12))
            fig.suptitle(title, size=25)
            ax1 = fig.add_subplot(2,2,1)
            ax2 = fig.add_subplot(2,2,2)
            ax3 = fig.add_subplot(2,2,3)
            ax4 = fig.add_subplot(2,2,4, projection='3d')
            #-------------------------------------------------------
            ax1.plot(self.y[0],self.y[1])
            ax1.set_xlabel('x')
            ax1.set_ylabel('y')
            #-------------------------------------------------------
            ax2.plot(self.y[0],self.y[2])
            ax2.set_xlabel('x')
            ax2.set_ylabel('z')
            #-------------------------------------------------------
            ax3.plot(self.y[1],self.y[2])
            ax3.set_xlabel('y')
            ax3.set_ylabel('z')  
            #-------------------------------------------------------          
            ax4.plot(self.y[0],self.y[1],self.y[2])
            ax4.set_xlabel('x')
            ax4.set_ylabel('y')
            ax4.set_zlabel('z')
            #-------------------------------------------------------
            caption = r'''Initial conditions and parameters
$x_0 = {}$  $y_0 = {}$  $z_0 = {}$
'''.format(self.y0[0],self.y0[1],self.y0[2],)
            param_list = r''

            for i in range(len(self.func_params)):
                param_list += r'${} = {}\,\,$'.format(parms_names[i],self.func_params[i])
            
            ax3.text(1.1,-0.25,caption+param_list,transform=ax3.transAxes,fontsize=13,horizontalalignment='center')
            
            plt.savefig('./'+ name +'.png')
        else:
            print('Problem has not been solved, call soluciones() method')
#+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
#The following portion of code only executes when this executable is run directly from the console
#So, when imported from __main__.py archive, this is not executed
#+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

#this portion is for the inheritance numeral, here we use RGKA to genereate the solution and desired plot.
if __name__ == '__main__':
    problem = RKGA(function=lambda x, y ,p: x-y,initial_conditions= [0,0],x_final=1,step=0.00001,func_params=[])  # single differential equation
    problem.plot(analytic_sol=lambda x: np.exp(-x) + x - 1, value = True) # comparison with its analitic solution
#+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++