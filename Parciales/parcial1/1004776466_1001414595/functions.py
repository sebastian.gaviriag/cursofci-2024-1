import numpy as np

def lorenz(t,variables:list,parameters:list) -> tuple:
    '''
    Function that define the Lorentz's atractor
    '''
    x,y,z = variables[0],variables[1],variables[2]
    sigma,rho,B = parameters[0],parameters[1],parameters[2]

    dx = sigma * ( y - x)
    dy = x * ( rho - z ) - y
    dz = x*y - B*z
    return [dx,dy,dz]

def dif_eq(x,y,param):
    dy = x - y
    return dy