# Examen parcial 1
### Fisica computacional 1
### Juan Sebastian Guarnizo - Mario José Félix Rojas


La entrega de este examen está diseñada de la siguiente manera, hay una carpeta para cada resultado de los puntos, es decir, en la carpeta Punto_1 se encuentra el archivo requirements.txt, que es donde están utilizadas las librerías requeridas para la correcta ejecución del proyecto. En la carpeta Punto_2 se encuentra la gráfica generada por la función con herencia de clases, el nombre de esta gráfica es punto2_herencia_graficas.png. En la carpeta Punto_3, se encuentra la carpeta Imagenes que es donde se guardan las gráficas generadas en cada uno de los casos, el formato de nombre para cada gráfica es el siguiente:
+ Si es gráfica 3D tenemos: fig_3d_x0_y0_z0_sigma_rho_beta.png
+ Si es gráfica 2D tenemos: fig_2d_var1_vs_var2_x0_y0_z0_sigma_rho_beta.png
## 1. Configuración de entorno de trabajo (10%)

Para una correcta ejecución del proyecto se necesita python en la versión 3.X, dentro de los requirements.txt únicamente necesita matplotlib para graficación y numpy para operaciones numéricas y manejo de variables.
### Requisitos:
+ Python 3.x
+ Numpy
+ Matplotlib

## 2. POO (40%)

Desarrollo de la clase RKG que permite calcular las soluciones de una EDO de grado 1, siguiendo un método de Runge-Kutta generalizado y teniendo en cuenta lo siguiente:

    2.1 Atributos
    2.2 Metodos
    2.3 Herencia

## 3. Solución numérica de ecuaciones diferenciales (40%)

Se utilizan las clases desarrolladas anteriormente en el punto anterior para solucionar las ecuaciones de Lorenz

$$ \frac{dx}{dt}= \sigma (y-x)$$
$$ \frac{dy}{dt} = x(\rho -z )-y$$
$$ \frac{dz}{dt} = xy-\beta z$$

Con un tiempo límite dado por t = 100. Para lo siguientes casos:

1. $x_0=0.0, y_0=0.0, z_0=0.0$ y $\sigma = 10, \rho = 28$ y $\beta = 8/3$
2. $x_0=0.0, y_0=0.01, z_0=0.0$ y $\sigma = 10, \rho = 28$ y $\beta = 8/3$
3. $x_0=0.0, y_0=0.0, z_0=0.0$ y $\sigma = 16, \rho = 45$ y $\beta = 4$
4. $x_0=0.0, y_0=0.001, z_0=0.0$ y $\sigma = 16, \rho = 45$ y $\beta = 4$


Para solucionar este problema consideramos las clases desarrolladas anteriormente, RKG y RKGA, en el caso en que el método derivada se toma según las ecuaciones de Lorenz.

