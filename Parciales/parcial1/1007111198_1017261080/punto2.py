# Preguntar :
import numpy as np
import matplotlib.pyplot as plt
from scipy.integrate import odeint

class RKG:
    """
    Esta clase permite calcular las soluciones de una Ecuación Diferencial Ordinaria (EDO)
    de grado 1 utilizando el método de Ruge-Kutta generalizado.

    Atributos:
        - derivada: Función que define la derivada y' = f(x,y).
        - x0: Valor inicial de la variable independiente.
        - y0: Valor inicial de la variable dependiente.
        - xf: Valor final de la variable independiente.
        - h: Paso de cada iteración.
        - constantes_butcher: Array de constantes de Butcher asociadas al orden del método RK.

    Métodos:
        - __init__(derivada, x0, y0, xf, h, constantes_butcher): Constructor de la clase RKG.
        - ks(x, y): Calcula los valores de k para el método de Runge-Kutta.
        - soluciones(): Resuelve la EDO utilizando el método de Runge-Kutta y devuelve
          arrays con los valores de x y y.
    """

    def __init__(self, derivada, x0, y0, xf, h, constantes_butcher):
        """
        Constructor de la clase RKG.

        Args:
            derivada (función): Función que define la derivada y' = f(x,y).
            x0 (float): Valor inicial de la variable independiente.
            y0 (float): Valor inicial de la variable dependiente.
            xf (float): Valor final de la variable independiente.
            h (float): Paso de cada iteración.
            constantes_butcher (array): Array de constantes de Butcher asociadas al orden del método RK.
                Debe tener 3 filas: en la primera fila deben estar las constantes a, en la segunda
                las b y en la tercera las c.
        """
        self.derivada = derivada
        self.x0 = x0
        self.y0 = y0
        self.xf = xf
        self.h = h
        self.a = constantes_butcher[0]
        self.b = constantes_butcher[1]
        self.c = constantes_butcher[2]

    def ks(self, x, y):
        """
        Calcula los diferentes valores de k para el método de Runge-Kutta.

        Args:
            x (float): Valor de la variable independiente.
            y (float): Valor de la variable dependiente.

        Returns:
            list: Lista con los diferentes valores de k calculados.
        """
        k = []
        for i in range(len(self.b)):
            xi = x + self.h * self.c[i]
            yi = y
            for j in range(i):
                yi = yi + self.a[i][j] * k[j]
            k.append(self.h * self.derivada(xi, yi))
        return k

    def soluciones(self):
        """
        Resuelve la EDO utilizando el método de Runge-Kutta.

        Returns:
            tuple: Tupla de arrays con los valores de x y y.
        """
        x = np.arange(self.x0, self.xf + self.h, self.h)
        y = [self.y0]
        for i in x[:-1]:
            k = self.ks(i, y[-1])
            pendiente_ponderada = 0
            for bi, ki in zip(self.b, k):
                pendiente_ponderada = pendiente_ponderada + bi * ki
            y.append(y[-1] + pendiente_ponderada)
        return x, y
    

class RKGA(RKG):
    """
    Clase que hereda de RKG y agrega funcionalidades para graficar las soluciones
    numéricas obtenidas mediante el método de Runge-Kutta generalizado y compararlas
    con las soluciones obtenidas utilizando la función odeint de scipy.

    Atributos heredados de RKG:
        - derivada: Función que define la derivada y' = f(x,y).
        - x0: Valor inicial de la variable independiente.
        - y0: Valor inicial de la variable dependiente.
        - xf: Valor final de la variable independiente.
        - h: Paso de cada iteración.
        - constantes_butcher: Array de constantes de Butcher asociadas al orden del método RK.

    Métodos:
        - __init__(derivada, x0, y0, xf, h, constantes_butcher): Constructor de la clase RKGA.
        - graficas_solicitadas(): Genera y muestra gráficos que comparan la solución numérica
          obtenida mediante RKG con la solución obtenida mediante odeint, además de mostrar
          la convergencia de las soluciones.
    """

    def __init__(self, derivada, x0, y0, xf, h, constantes_butcher):
        """
        Constructor de la clase RKGA.

        Args:
            derivada (función): Función que define la derivada y' = f(x,y).
            x0 (float): Valor inicial de la variable independiente.
            y0 (float): Valor inicial de la variable dependiente.
            xf (float): Valor final de la variable independiente.
            h (float): Paso de cada iteración.
            constantes_butcher (array): Array de constantes de Butcher asociadas al orden del método RK.
                Debe tener 3 filas: en la primera fila deben estar las constantes a, en la segunda
                las b y en la tercera las c.
        """
        super().__init__(derivada, x0, y0, xf, h, constantes_butcher)
        
    def soluciones(self):
        x = np.arange(self.x0, self.xf + self.h, self.h)
        y = [self.y0]  # y0 es ahora un vector de condiciones iniciales.
        for i in x[:-1]:
            k = self.ks(i, y[-1])
            pendiente_ponderada = np.zeros_like(self.y0)        # pues va haber una por variable depndnt
            for bi, ki in zip(self.b, k):
                pendiente_ponderada = pendiente_ponderada + bi * ki
            y.append(y[-1] + pendiente_ponderada)               # guardamos y_2 = y_1 + m*(x2-x1) con x2-x1 = h = cte (h se pone en ks), x 1-dim y y 3-dim
        return x, np.array(y)
    
    def graficas_solicitadas(self):
        """
        Genera y muestra gráficos que comparan la solución numérica obtenida mediante
        RKG con la solución obtenida mediante odeint, además de mostrar la convergencia
        de las soluciones.

        No recibe argumentos.
        """
        h = self.h
        x, y = self.soluciones()
        y_ref = odeint(lambda t, x: self.derivada(x, t), self.y0, x).flatten()
        
        # Gráfico de la solución
        plt.rcParams['figure.figsize'] = 20,5  #ancho por alto 18 y 5
        plt.subplot(1,3,1)
        plt.plot(x, y, label='Solución numérica')
        plt.xlabel('x')
        plt.ylabel('y')
        plt.title('Solución de la EDO usando RKG con h={h}')
        plt.legend()

        # Comparacion
        plt.subplot(1,3,2)
        plt.plot(x, y_ref, label='Solución odeint', linestyle='--')
        plt.plot(x, y, label='Solución numérica')
        plt.xlabel('x')
        plt.ylabel('y')
        plt.title('Comparación de la solución RKG con odeint con h={h}')
        plt.legend()

        # Convergencia de las soluciones comparativamente con la solucion analitica
        plt.subplot(1,3,3)
        plt.plot(x, abs(np.array(y_ref) - np.array(y)), label='Solución numérica')
        plt.xlabel('x')
        plt.ylabel('y')
        plt.title('Convergencia de las soluciones de manera comparativa, con la solucion analitica h={h}')
        plt.legend()
        plt.savefig('./Punto_2/punto2_'+str(h)+'herencia_graficas.png')
        #plt.show()

