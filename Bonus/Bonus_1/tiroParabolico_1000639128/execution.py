#!./venv/bin/python
from tiroParabolico import tiroParabolico
import numpy as np

if __name__=="__main__":
    angle = np.pi/4
    velocity = 10 #en magnitud
    inicial_pos = [0,10] #[x0,y0]

    tiro = tiroParabolico(angle,velocity,inicial_pos)
    tiro.drawTraject(3) #dibuja la trayectoria por 3 veces la unidad de tiempo