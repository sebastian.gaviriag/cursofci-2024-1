import numpy as np
import matplotlib.pyplot as plt
from tiro_parabolico_1000439477  import tiroparabolico

if __name__ == '__main__':
    tiro=tiroparabolico(-9.8,np.pi/6,10,2,0)
    
    t=np.arange(0,tiro.tiempo_max_vuelo()+0.05,0.05)
    plt.plot(tiro.pos_x(t),tiro.pos_y(t))
    plt.savefig("tiroparabolico.png")
    plt.show()