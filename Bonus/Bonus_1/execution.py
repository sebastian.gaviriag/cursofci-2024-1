from TiroParabolico import tiroParabolico, plotTiroParabolico

if __name__=='__main__':
    x0 = 0 #Posición horizontal inicial
    y0 = 1 #Posición vertical inicial
    v0 = 20 #Magnitud de la velocidad inicial
    angulo = 45
    tiro = tiroParabolico(x0, y0, v0, angulo)
    graficador = plotTiroParabolico(tiro)
    graficador.plot()