import numpy as np 
import matplotlib.pyplot as plt

class tiroParabolico:
    """"" En esta clase se resolverá el problema del tiro parabólico.
    Se mostrará la gráfica de la trayectoria de la partícula para diferentes condiciones iniciales.
    """
    alpha=0 #Ángulo de tiro
    V0=0    #Velocidad inicial
    g=9.8   #m/s**2
    posicion0=[0,0] #Posición inicial
    
    def __init__(self,_alpha,_V0,_posicion0):
        self.alpha=_alpha
        self.V0=_V0
        self.posicion0=_posicion0
        self.alpha = np.radians(_alpha)
        self.t=np.linspace(0,2,100)

    def Velocidad(self):
        vx=self.V0*np.cos(self.alpha)*len(self.t)
        vy=self.V0*np.sin(self.alpha)-self.g*self.t
        
        return vx,vy
    
    def Posicion(self):
        x = self.posicion0[0] + self.V0 * np.cos(self.alpha) * self.t
        y = self.posicion0[1] + self.V0 * np.sin(self.alpha) * self.t - 0.5 * self.g * self.t**2
        return x,y
    
    def T_v(self):
        return (-((self.V0*np.sin(self.alpha))**2-2*self.g*self.posicion0[1])**(0.5)-(self.V0*np.sin(self.alpha)))/self.g

    def grafico_posicion(self):
        x, y = self.Posicion()
        plt.plot(x, y)
        plt.xlabel('Posición en x')
        plt.ylabel('Posición en y')
        plt.title('Gráfico de Trayectoria')
        plt.grid(True)
        plt.show()


class tiroParabolico2(tiroParabolico):
        
    def __init__(self,_alpha,_V0,_posicion0,ax):
        super().__init__(_alpha,_V0,_posicion0)
        self.ax=ax
            
    def Posicion(self):
        x = self.posicion0[0] + self.V0 * np.sin(self.alpha) * self.t + 0.5 * self.ax * self.t**2
        y = self.posicion0[1] + self.V0 * np.sin(self.alpha) * self.t - 0.5 * self.g * self.t**2
        return x,y