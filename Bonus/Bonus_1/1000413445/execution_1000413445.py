import numpy as np
import matplotlib.pyplot as plt
from tiroparabolico_1000413445 import tiroParabolico

if __name__ == '__main__':
    theta = np.random.uniform(0,90) # °
    v0 = np.random.uniform(5,25) # m/s
    g = 9.8 # m/s²
    r0 = np.random.uniform(0,5,2) # m

    movimiento = tiroParabolico(theta,v0,g,r0)
    print(f'Tiempo de vuelo = {movimiento.tV} s')
    print(movimiento.velocidad())
    print(movimiento.posicion())
    print(movimiento.trayectoria())