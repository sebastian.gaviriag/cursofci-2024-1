import numpy as np
import matplotlib.pyplot as plt

class tiroParabolico:
    """
    G: gravedad
    angle: ángulo inicial de salida
    v0: magnitud de la velocidad inicial de salida
    y_0: altura inicial
    x_0: posición inicial en x
    """

    def __init__(self, gravity, angle, v_0, h_0, x_0, dt=0.01):
        self.G=gravity
        self.angle=angle
        self.v0=v_0
        self.y0=h_0
        self.x0=x_0
        self.t=np.arange(0,self.tiempo_max_vuelo(),dt)

    def vel_x(self):
        return self.v0*np.cos(self.angle)

    def vel_y(self):
        return self.v0*np.sin(self.angle)+self.G*self.t
    
    def pos_x(self):
        return self.v0*np.cos(self.angle)*self.t+self.x0
    
    def pos_y(self):
        return self.v0*np.sin(self.angle)*self.t+(self.G/2)*(self.t**2)+self.y0 

    def tiempo_max_vuelo(self):
        return 2*(-self.v0)*np.sin(self.angle)/self.G
    
    def plot_parabolico(self):
        plt.plot(self.pos_x(),self.pos_y())
        plt.xlabel('x [m]')
        plt.ylabel('y [m]')
        plt.title('Trayectoria parabólica')
        plt.savefig('Plots/tiroParabolico.png')
        plt.show()

class tiroParabolicoConViento(tiroParabolico):
    def __init__(self,gravity,angle,velini,h_0,x_0,a_air):
        super().__init__(gravity,angle,velini,h_0,x_0)
        self.ax=a_air
    
    def pos_x(self):
        return self.v0*np.cos(self.angle)*self.t+(self.ax/2)*(self.t**2)+self.x0 